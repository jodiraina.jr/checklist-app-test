import React, { useEffect, useState } from "react";
import axios from "axios";
import { useParams, useNavigate, useLocation } from "react-router-dom";

const ChecklistItemEdit = () => {
  const navigate = useNavigate();
  const { id, itemid } = useParams();
  const { state } = useLocation();
  const [itemName, setItemName] = useState(state.name);

  const store = () => {
    const token = localStorage.getItem("token");
    let config = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    const data = {
      itemName,
    };

    axios
      .put(
        `http://94.74.86.174:8080/api/checklist/${id}/item/rename/${itemid}`,
        data,
        config
      )
      .then((res) => {
        setItemName("");
        navigate(-1);
      })
      .catch((err) => {});
  };

  useEffect(() => {}, []);

  return (
    <div className="d-flex flex-column justify-center pa-4">
      <div className="mb-4">
        <input
          placeholder="name"
          value={itemName}
          onChange={(event) => setItemName(event.target.value)}
          type="text"
          className="mr-3"
        ></input>
        <button disabled={itemName ? false : true} onClick={store}>
          Ubah Nama Item
        </button>
      </div>
    </div>
  );
};

export default ChecklistItemEdit;
