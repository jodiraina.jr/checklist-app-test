import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const Register = () => {
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const register = () => {
    const env = process.env.BASE_URL;
    const data = {
      email,
      username,
      password,
    };

    axios
      .post(`http://94.74.86.174:8080/api/register`, data)
      .then((res) => {
        console.log(res.data);
        navigate(-1);
      })
      .catch((err) => {
        alert(err.message);
      });
  };

  return (
    <div className="d-flex flex-column align-center justify-center">
      <div className="mb-3 mt-3 d-flex flex-column">
        <input
          placeholder="email"
          value={email}
          onChange={(event) => setEmail(event.target.value)}
          type="text"
          className="mb-3"
        ></input>
        <input
          placeholder="username"
          value={username}
          onChange={(event) => setUsername(event.target.value)}
          type="text"
          className="mb-3"
        ></input>
        <input
          placeholder="password"
          value={password}
          onChange={(event) => setPassword(event.target.value)}
          type="password"
          className="mb-3"
        ></input>
      </div>
      <button onClick={register}>Daftar</button>
      <a href="/">Kembali</a>
    </div>
  );
};

export default Register;
