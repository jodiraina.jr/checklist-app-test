import React, { useEffect, useState } from "react";
import axios from "axios";
import { useParams, useNavigate } from "react-router-dom";

const Checklist = () => {
  const navigate = useNavigate();
  const [itemName, setItemName] = useState("");
  const [data, setData] = useState([]);
  const { id } = useParams();

  const store = () => {
    const token = localStorage.getItem("token");
    let config = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    const data = {
      itemName,
    };

    axios
      .post(`http://94.74.86.174:8080/api/checklist/${id}/item`, data, config)
      .then((res) => {
        getChecklistItem();
        setItemName("");
      })
      .catch((err) => {});
  };

  const remove = (itemId) => {
    const token = localStorage.getItem("token");
    let config = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    axios
      .delete(
        `http://94.74.86.174:8080/api/checklist/${id}/item/${itemId}`,
        config
      )
      .then((res) => {
        getChecklistItem();
      })
      .catch((err) => {});
  };

  const status = (itemId) => {
    const token = localStorage.getItem("token");
    let config = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    axios
      .put(
        `http://94.74.86.174:8080/api/checklist/${id}/item/${itemId}`,
        "",
        config
      )
      .then((res) => {
        getChecklistItem();
      })
      .catch((err) => {});
  };

  const getChecklistItem = () => {
    const token = localStorage.getItem("token");
    let config = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    axios
      .get(`http://94.74.86.174:8080/api/checklist/${id}/item`, config)
      .then((res) => {
        const data = res.data.data;
        console.log(data);
        setData(data);
      })
      .catch((err) => {});
  };

  useEffect(() => {
    getChecklistItem();
  }, []);

  return (
    <div className="d-flex flex-column justify-center pa-4">
      <div className="mb-4">
        <input
          placeholder="name"
          value={itemName}
          onChange={(event) => setItemName(event.target.value)}
          type="text"
          className="mr-3"
        ></input>
        <button disabled={itemName ? false : true} onClick={store}>
          Buat Checklist Item Baru
        </button>
      </div>

      {data.map((item) => {
        return (
          <div key={item.id} style={{ width: "500px" }} className="mb-3">
            <div className="d-flex flex justify-between">
              <p className="mr-3">{item.name}</p>
              <button className="mr-3" onClick={() => remove(item.id)}>
                Hapus
              </button>
              <button
                className="mr-3"
                onClick={() =>
                  navigate(`/checklist-item/${id}/edit/${item.id}`, {
                    state: { name: item.name },
                  })
                }
              >
                Ubah
              </button>
              <input
                type="checkbox"
                checked={item.itemCompletionStatus}
                onChange={() => status(item.id)}
              ></input>
              <p>{item.itemCompletionStatus ? "Selesai" : "Belum"}</p>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default Checklist;
