import React, { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const Checklist = () => {
  const navigate = useNavigate();
  const [name, setName] = useState("");
  const [data, setData] = useState([]);

  const store = () => {
    const token = localStorage.getItem("token");
    let config = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    const data = {
      name,
    };

    axios
      .post(`http://94.74.86.174:8080/api/checklist`, data, config)
      .then((res) => {
        getChecklist();
        setName("");
      })
      .catch((err) => {});
  };

  const remove = (id) => {
    const token = localStorage.getItem("token");
    let config = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    axios
      .delete(`http://94.74.86.174:8080/api/checklist/${id}`, config)
      .then((res) => {
        getChecklist();
      })
      .catch((err) => {});
  };

  const getChecklist = () => {
    const token = localStorage.getItem("token");
    let config = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };

    axios
      .get(`http://94.74.86.174:8080/api/checklist`, config)
      .then((res) => {
        const data = res.data.data;
        setData(data);
      })
      .catch((err) => {});
  };

  useEffect(() => {
    getChecklist();
  }, []);

  return (
    <div className="d-flex flex-column justify-center pa-4">
      <div className="mb-4">
        <input
          placeholder="name"
          value={name}
          onChange={(event) => setName(event.target.value)}
          type="text"
          className="mr-3"
        ></input>
        <button disabled={name ? false : true} onClick={store}>
          Buat Checklist Baru
        </button>
      </div>

      {data.map((item) => {
        return (
          <div key={item.id} style={{ width: "500px" }} className="mb-3">
            <div className="d-flex flex justify-between">
              <p className="mr-3">{item.name}</p>
              <button className="mr-3" onClick={() => remove(item.id)}>
                Hapus
              </button>
              <button
                className="mr-3"
                onClick={() => navigate(`/checklist-item/${item.id}`)}
              >
                Detail
              </button>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default Checklist;
