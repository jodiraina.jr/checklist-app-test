import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const Login = () => {
  const navigate = useNavigate();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const login = () => {
    const env = process.env.BASE_URL;
    const data = {
      username,
      password,
    };

    axios
      .post(`http://94.74.86.174:8080/api/login`, data)
      .then((res) => {
        const token = res.data.data.token;
        localStorage.setItem("token", token);
        navigate("/Checklist");
      })
      .catch((err) => {
        alert(err.message);
      });
  };

  return (
    <div className="d-flex flex-column align-center justify-center">
      <div className="mb-3 mt-3 d-flex flex-column">
        <input
          placeholder="username"
          value={username}
          onChange={(event) => setUsername(event.target.value)}
          type="text"
          className="mb-3"
        ></input>
        <input
          placeholder="password"
          value={password}
          onChange={(event) => setPassword(event.target.value)}
          type="password"
          className="mb-3"
        ></input>
      </div>
      <button onClick={login}>Login</button>
      <a href="/Register">Belum punya akun?</a>
    </div>
  );
};

export default Login;
