import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Login from "../pages/Login";
import Register from "../pages/Register";
import Checklist from "../pages/Checklist";
import ChecklistItem from "../pages/ChecklistItem";
import ChecklistItemEdit from "../pages/ChecklistItemEdit";
import NotFound from "../pages/NotFound";

const RouterSetup = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/Register" element={<Register />} />
        <Route path="/Checklist" element={<Checklist />} />
        <Route path="/checklist-item/:id" element={<ChecklistItem />} />
        <Route
          path="/checklist-item/:id/edit/:itemid"
          element={<ChecklistItemEdit />}
        />

        {/* etc */}
        <Route path="*" element={<NotFound />} />
      </Routes>
    </Router>
  );
};

export default RouterSetup;
