import { Navigate, Outlet } from "react-router-dom";

const AuthRoute = () => {
  const token = localStorage.get("token");

  return token ? <Outlet /> : <Navigate to={"/login"} />;
};
export default AuthRoute;
