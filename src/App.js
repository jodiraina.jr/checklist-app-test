import "./App.css";
import RouterSetup from "./router";

function App() {
  return (
    <div className="App">
      <RouterSetup />
    </div>
  );
}

export default App;
